use std::io::{Stdout, Write};
use crossterm::{QueueableCommand, 
                terminal::{Clear, ClearType},
                style::{SetBackgroundColor, Color},
                cursor::MoveTo};

use crate::frame::Frame;

/*
Optimization: only render the differences.
But at least once we will need to render everything, so we add the option to force it.
*/
pub fn render(stdout: &mut Stdout, last_frame: &Frame, curr_frame: &Frame, force: bool) {
    if force {
        // Queue a bunch of commands that will be executed later
        stdout.queue(SetBackgroundColor(Color::Blue)).unwrap();
        stdout.queue(Clear(ClearType::All)).unwrap();
        stdout.queue(SetBackgroundColor(Color::Black)).unwrap();
    }
    for (x, col) in curr_frame.iter().enumerate() {
        for (y, s) in col.iter().enumerate() {
            if *s != last_frame[x][y] || force {
                stdout.queue(MoveTo(x as u16, y as u16)).unwrap();
                print!("{}", *s);
            }
        }
    }
    stdout.flush().unwrap();
}
