use crossterm::{ExecutableCommand,
                cursor::{Hide, Show},
                event::{self, Event, KeyCode},
                terminal::{self, EnterAlternateScreen, LeaveAlternateScreen}};
use rusty_audio::Audio;
use rusty_invaders::{frame::{self, new_frame, Drawable},
                     render,
                     player::Player, 
                     invaders::Invaders};
use std::{error::Error,
          io,
          thread,
          sync::mpsc,
          time::{Duration, Instant}};
/* 
    Returns a Result<T, E> so that it is possible to us "?" ergonomically.
    We don't care about the Type for success (T).
    For the type of Err (E) use a Box with dynamic Error.
    
    Box<T>, casually referred to as a ‘box’, provides the simplest form of heap allocation in Rust. 
    Boxes provide ownership for this allocation, and drop their contents when they go out of scope. 
    Boxes also ensure that they never allocate more than isize::MAX bytes
*/
fn main() -> Result <(), Box<dyn Error>>{
    /* Setup */
    // Audio
    let mut audio = Audio::new();
    audio.add("explode", "sounds/explode.wav");
    audio.add("lose", "sounds/lose.wav");
    audio.add("move", "sounds/move.wav");
    audio.add("pew", "sounds/pew.wav");
    audio.add("startup", "sounds/startup.wav");
    audio.add("win", "sounds/win.wav");
    audio.play("startup");

    // Terminal
    let mut stdout = io::stdout();
    terminal::enable_raw_mode()?; // ? operators will cause crash if there is an error
    stdout.execute(EnterAlternateScreen)?; // Similar to how VIM enters its own screen.
    stdout.execute(Hide)?; // hide the mouse


    /* Render loop in a separate thread */
    let (render_tx, render_rx) = mpsc::channel();
    let render_handle = thread::spawn(move || {
        let mut last_frame = frame::new_frame();
        let mut stdout = io::stdout();
        render::render(&mut stdout, &last_frame, &last_frame, true);
        loop {
            let curr_frame = match render_rx.recv() {
                Ok(x) => x,
                Err(_) => break,
            };
            render::render(&mut stdout, &last_frame, &curr_frame, false);
            last_frame = curr_frame;
        }
    });
    

    /* Game Loop */
    let mut player = Player::new();
    let mut instant = Instant::now();
    let mut invaders = Invaders::new();
    'gameloop: loop {
        // Per-frame init
        let delta = instant.elapsed();
        instant = Instant::now();
        let mut curr_frame = new_frame();

        // Input
        while event::poll(Duration::default())? {
            if let Event::Key(key_event) = event::read()? {
                match key_event.code {
                    KeyCode::Left => player.move_left(),
                    KeyCode::Right => player.move_right(),
                    KeyCode::Char(' ') | KeyCode::Enter => {
                        if player.shoot() {
                            audio.play("pew");
                        }
                    }
                    KeyCode::Esc | KeyCode::Char('q') => {
                        audio.play("lose");
                        break 'gameloop;
                    },
                    _ => {}
                }
            }
        }

        // Updates
        player.update(delta);
        if invaders.update(delta) {
            audio.play("move");
        }
        if player.detect_hits(&mut invaders) {
            audio.play("explode");
        }

        // Draw and render
        let drawables: Vec<&dyn Drawable> = vec![&player, &invaders];
        for drawable in drawables {
            drawable.draw(&mut curr_frame);
        }
        let _ = render_tx.send(curr_frame); // silently ignore errores that will come from the child thread not being started.
        thread::sleep(Duration::from_millis(1)); // game loop is much faster than render loop.

        // Win or lose?
        if invaders.all_killed() {
            audio.play("win");
            break 'gameloop;
        }
        if invaders.reached_bottom() {
            audio.play("lose");
            break 'gameloop;
        }
    }

    /* Cleanup */
    drop(render_tx);
    render_handle.join().unwrap();

    // Audio system plays audio in second thread so lets sync before exiting:
    audio.wait();

    // Restore terminal
    stdout.execute(Show)?;
    stdout.execute(LeaveAlternateScreen)?;
    terminal::disable_raw_mode()?;

    // Return
    Ok(())
}
