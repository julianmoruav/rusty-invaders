use crate::{NUM_COLS, NUM_ROWS};

// Create a type that makes it easy to create frames
pub type Frame =  Vec<Vec<&'static str>>;

/* 
Simply a vector of vectors of burrowed string slices
Fill up a matrix of space char.
*/ 
pub fn new_frame() -> Frame {
    let mut cols = Vec::with_capacity(NUM_COLS);
    for _ in 0..NUM_COLS {
        let mut col =  Vec::with_capacity(NUM_ROWS);
        for _ in 0..NUM_ROWS {
            col.push(" ");
        }
        cols.push(col);
    }
    cols
}

/*
Everything that we want to see needs to be able to draw itself into the frme
Create a Drawable trait for this
*/
pub trait Drawable {
    fn draw(&self, frame: &mut Frame);
}

