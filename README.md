
# rusty-invaders

The famous Space Invaders game, written in Rust.
This project is developed for eduactional purposes as part of the Ultimate Rust Crash Course by Nathan Stocks. 

## Install, Update or Uninstall Rust

* Install `rustup`. Restart the shell after it is done. More info [here](https://www.rust-lang.org/tools/install)
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

* The Rust toolchain is found under `~/.cargo/bin/`. Configuring the `PATH` environment variable might be needed.

```bash
sudo vim /etc/environment
```

* Update `rustup` installation. Documentation [here](https://github.com/rust-lang/rustup.rs/blob/master/README.md).

```bash
rustup update
```

* Uninstall Rust.
```bash
rustup self uninstall
```

## Install Dependencies

On Ubuntu:

```
sudo apt install libasound2-dev pkg-config
```

## Run the Game

At the root of the project:

```
cargo run
```

## References

1. [Ultimate Rust Crash Course (Udemy)](https://www.udemy.com/course/ultimate-rust-crash-course/)
2. [Rusty Engine for 2D Game Development in Rust](https://github.com/cleancut/rusty_engine)
3. [Rust in VSCode](https://code.visualstudio.com/docs/languages/rust)

